package com.reqres.api;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class ApiTest {

    @Test
    public void testSimple() {
        given()
                .contentType(ContentType.JSON)
        .when()
            .get("https://reqres.in/api/users")
        .then()
            .statusCode(200)
            .log().all();
    }

    @Test
    public void testForMatch() {
        given()
            .contentType(ContentType.JSON)
        .when()
            .get("https://reqres.in/api/users")
        .then()
            .statusCode(200)
            .body("page", equalTo(1),
                    "page", equalTo( 1),
                    "data.id", hasItems( 3,4))
            .log().all();
    }
}
