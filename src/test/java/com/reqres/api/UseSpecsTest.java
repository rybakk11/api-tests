package com.reqres.api;

import com.mapping.Page;
import com.mapping.Person;
import com.mapping.User;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class UseSpecsTest {

    RequestSpecification rqSpec;
    ResponseSpecification rsSpec;

    @BeforeClass
    public void setUp() {
        rqSpec = new RequestSpecBuilder()
                .setBaseUri("https://reqres.in")
                .setAccept(ContentType.JSON)
                .setContentType(ContentType.JSON)
                .log(LogDetail.ALL)
                .build();

        rsSpec = new ResponseSpecBuilder()
                .log(LogDetail.ALL)
                .build();

        RestAssured.requestSpecification = rqSpec;
        RestAssured.responseSpecification = rsSpec;
    }

    @Test(description = "Проверка, что все поля пришли.")
    public void testGetRq() {
        Page page = given()
                .when()
                    .get("/api/users?page=2")
                .then()
                    .statusCode(200).log().all()
                    .extract()
                    .body()
                    .as(Page.class);
        for (User user : page.getData()) {
            Assert.assertNotNull(user.getId(), String.format("Поле id заполнено", user.getId()));
            Assert.assertNotNull(user.getEmail(), String.format("Для пользователя %s поле email заполнено", user.getId()));
            Assert.assertNotNull(user.getFirstName(), String.format("Для пользователя %s поле first_name заполнено", user.getId()));
            Assert.assertNotNull(user.getLastName(), String.format("Для пользователя %s поле last_name заполнено", user.getId()));
            Assert.assertNotNull(user.getAvatar(), String.format("Для пользователя %s поле avatar заполнено", user.getId()));
        }
    }

    @Test(description = "Создание пользователя его отправка и проверка ответа.")
    public void testPostRq() {
        Person sendPerson = new Person("Nico", "manager");
        Person receivedPerson = given()
                .contentType(ContentType.JSON).body(sendPerson)
                .when().post("/api/users")
                .then().log().all().statusCode(201).extract().body().as(Person.class);
        Assert.assertEquals(sendPerson, receivedPerson, "Значения ответа и запроса совпадают");
    }

    @Test(description = "Обновление пользователя.")
    public void testPutRq() {
        Person sendPerson = new Person("Nico", "manager");
        Person receivedPerson = given()
                .contentType(ContentType.JSON).body(sendPerson)
                .when().put("/api/users")
                .then().log().all().statusCode(200).extract().body().as(Person.class);
        Assert.assertEquals(sendPerson, receivedPerson, "Значения ответа и запроса совпадают");
    }

    @Test(description = "Удаление пользователя.")
    public void testDeleteRq() {
        given()
                .contentType(ContentType.JSON)
                .when().delete("/api/users/25")
                .then().log().all().statusCode(204);
    }
}
